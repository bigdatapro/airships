upnode = require("upnode")
attempt = require('attempt')

cities = 1: {x: 10, y: 10}, 2: {x: 10, y: 50}, 3: {x: 60, y: 10}
airShipSpeed = 10

toRadians = (angle) -> angle * (Math.PI / 180);

fc = upnode.connect("192.168.117.110", 3110)

getWeather = (date, cb) ->
  fc (remote) ->
    #console.log "Connected to forecast"
    protecter = ->
      timeoutProtect = null
      console.log 'forecast service timed out'
      cb 'forecast service timed out'
    timeoutProtect = setTimeout protecter, 10000
    remote.forecast date, (err, res) ->
      if timeoutProtect
        clearTimeout(timeoutProtect)
        console.dir forecast: res
        if err?
          cb err
        else if res?
          if res.azimuth?
            cb false, res
          else
            console.dir res
            cb true
        else
          console.log "Forecast service returned null"
          cb true

processOrder = (from, to, time, cb) ->
  if from == to
    cb false, true
  else
    attempt retries: 15, (-> getWeather time, this),
      (err, res) ->
        # console.log "got forecast"
        # console.dir err: err, res: res
        if err
          console.log "Forecast failed 15 times"
          cb "Forecast failed 15 times"
        else
          # console.dir forecast: {date: time, response: res}

          windAzimuth = res.azimuth
          windMagnitude = res.magnitude
          windX = Math.cos(toRadians(windAzimuth)) * windMagnitude
          windY = Math.sin(toRadians(windAzimuth)) * windMagnitude

          decision = windMagnitude < airShipSpeed
         
          cb false, decision

server = upnode((client, conn) ->
  @try = (data, cb) ->
    console.log request: data
    err = switch
      when !data? then "ERROR: Wrong request"
      when [1,2,3].indexOf(+data.from) == -1 then "ERROR: Check from id"
      when [1,2,3].indexOf(+data.to) == -1 then "ERROR: Check to id"
      else false
    if err
      console.dir error: err
      cb err
    else
      processOrder data.from, data.to, data.when, (err, result) ->
        if err
          console.dir err: err, request: data
          cb err
        else
          console.dir response: result, request: data
          cb false, result
)

port = 7010
server.listen port
console.log "Server started on port #{port}"
