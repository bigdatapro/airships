upnode = require("upnode")
attempt = require('attempt')

fc = upnode.connect("192.168.117.110", 3110)
getWeather = (date, cb) ->
  fc (remote) ->
    #console.log "Connected to forecast"
    protecter = ->
      timeoutProtect = null
      console.dir error: 'forecast service timed out'
      cb true
    timeoutProtect = setTimeout protecter, 1000
    console.dir date
    remote.forecast date, (err, res) ->
      if timeoutProtect
        clearTimeout(timeoutProtect)
        if err?
          cb err
        else if res?
          if res.azimuth?
            cb false, res
          else
            console.dir res
            cb true
        else
          console.log "Forecast service returned null"
          cb true
  
date = '2014-03-11T00:00:00.000Z'
attempt retries: 15, (-> 
  getWeather date, this),
  (err, res) ->
    if err
      console.log "Forecast failed 15 times"
    else
      console.dir forecast: {date: date, response: res}
